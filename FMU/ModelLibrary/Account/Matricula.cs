﻿using ModelLibrary.parameters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLibrary.Account
{
    public class Matricula
    {
        [DisplayName("Nome completo")]
        [Required(ErrorMessage = "Nome completo é um campo obrigatório")]
        public string Nome { get; set; }
        [DisplayName("Nome de usuário")]
        [Required(ErrorMessage = "Nome de usuário é um campo obrigatório")]
        public string Usuario { get; set; }
        [DisplayName("Senha")]
        [Required(ErrorMessage = "Senha é um campo obrigatório")]
        public string Senha { get; set; }
        [DisplayName("Confirmação")]
        [Required(ErrorMessage = "Confirmação é obrigatório")]
        [Compare("Senha", ErrorMessage = "Valores diferentes para o campo de senha.")]
        public string ConfSenha { get; set; }
        [DisplayName("Email")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail inválido")]
        [Required(ErrorMessage = "E-mail é um campo obrigatório")]
        public string Email { get; set; }
        [DisplayName("Cursos")]
        [Required(ErrorMessage = "Cursos é um campo obrigatório")]
        public List<Cursos> Cursos { get; set; }
        public string CursoInsideCode { get; set; }
    }
}
