﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLibrary.Account
{
    public class Login
    {
        [DisplayName("Nome de usuário")]
        [Required(ErrorMessage = "Nome de usuário é um campo obrigatório")]
        public string Username { get; set; }
        [DisplayName("Senha")]
        [Required(ErrorMessage = "Senha é um campo obrigatório")]
        public string Password { get; set; }
    }
}
