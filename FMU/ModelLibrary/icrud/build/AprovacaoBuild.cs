﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TkppAPI.IcrudTkppAPI.Helper;

namespace ModelLibrary.icrud.build
{
    public class AprovacaoBuild : IDisposable
    {
        DbHelper execute = new DbHelper();

        public void Dispose()
        {
            execute?.Dispose();
        }

        public SqlCommand MakeAprovacao() => execute.ExecuteTransaction("sp_list_aprovacao_aluno");

        public SqlCommand MakeAprovacao(List<int> list)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();

            var _data = new DataTable();
            _data.Columns.Add("RA", typeof(int));

            list.ForEach(x => _data.Rows.Add(x));

            parameters.Add(new SqlParameter("@LIST", _data) { SqlDbType = SqlDbType.Structured, TypeName = "dbo.LIST_APROV" });

            return execute.ExecuteTransaction("[SP_LIST_APROVACAO]", parameters);
        }
    }
}
