﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TkppAPI.IcrudTkppAPI.Helper;

namespace ModelLibrary.icrud.build
{
    public class GradeBuild : IDisposable
    {
        DbHelper execute = new DbHelper();

        public SqlCommand MakeGrade(int re)
        {

            List<SqlParameter> parameters = new List<SqlParameter>();

            parameters.Add(new SqlParameter("@RE", re));

            return execute.ExecuteTransaction("SP_EXIBIR_MATRIZ_ALUNO", parameters);
        }



        public void Dispose()
        {
            execute?.Dispose();
        }
    }
}
