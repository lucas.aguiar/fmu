﻿using ModelLibrary.Account;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TkppAPI.IcrudTkppAPI.Helper;

namespace ModelLibrary.icrud.build
{
    public class MatriculaBuild: IDisposable
    {
        DbHelper execute = new DbHelper();

        public SqlCommand MakeListDao() => execute.ExecuteTransaction("SP_LIST_CURSRO");
      
        public SqlCommand MakeCadastroAluno(Matricula model)
        {

            List<SqlCommand> cmds = new List<SqlCommand>();

            List<SqlParameter> parameters = new List<SqlParameter>();

            parameters.Add(new SqlParameter("@NOME", model.Usuario));
            parameters.Add(new SqlParameter("@EMAIL", model.Email));
            parameters.Add(new SqlParameter("@SENHA", model.Senha));
            parameters.Add(new SqlParameter("@CURSO", model.CursoInsideCode));


            return execute.ExecuteTransaction("SP_CAD_ALUNO", parameters);
        }

        public void Dispose()
        {
            execute?.Dispose();
        }

    }
}
