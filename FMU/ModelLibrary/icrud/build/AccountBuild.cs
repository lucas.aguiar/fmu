﻿using ModelLibrary.Account;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TkppAPI.IcrudTkppAPI.Helper;

namespace ModelLibrary.icrud.build
{
    public class AccountBuild: IDisposable
    {
        DbHelper execute = new DbHelper();

        public SqlCommand MakeLogin(Login model)
        {
            List<SqlCommand> cmds = new List<SqlCommand>();
          
            List<SqlParameter> parameters = new List<SqlParameter>();

            parameters.Add(new SqlParameter("@RA", model.Username));
            parameters.Add(new SqlParameter("@SENHA_ALUNO", model.Password));

            return execute.ExecuteTransaction("SP_LOGIN_ALUNO", parameters);
        }


        public void Dispose()
        {
            execute?.Dispose();
        }
    }
}
