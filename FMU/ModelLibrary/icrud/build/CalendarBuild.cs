﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TkppAPI.IcrudTkppAPI.Helper;

namespace ModelLibrary.icrud.build
{
    public class CalendarBuild : IDisposable
    {
        DbHelper execute = new DbHelper();

        public void Dispose()
        {
            execute?.Dispose();
        }

        public SqlCommand MakeCalendar(int? re)
        {
            if(re != null)
            {
                List<SqlParameter> parameters = new List<SqlParameter>();

                parameters.Add(new SqlParameter("@RE", re));

                return execute.ExecuteTransaction("SP_MAKER_GRADE_ALUNO", parameters);
            }
            else
            {
               return execute.ExecuteTransaction("SP_CALENDARIO");
            }

        }
    }
}
