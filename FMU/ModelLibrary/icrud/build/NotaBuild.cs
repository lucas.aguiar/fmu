﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TkppAPI.IcrudTkppAPI.Helper;

namespace ModelLibrary.icrud.build
{
    public class NotaBuild : IDisposable
    {
        DbHelper execute = new DbHelper();

        public void Dispose()
        {
            execute?.Dispose();
        }

        public SqlCommand MakeNota(int? re)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();

            parameters.Add(new SqlParameter("@RA", re));

            return execute.ExecuteTransaction("SP_NOTAS_FALTAS_GET", parameters);

        }
    }
}
