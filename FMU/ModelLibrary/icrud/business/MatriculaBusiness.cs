﻿using ModelLibrary.Account;
using ModelLibrary.parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLibrary.icrud.business
{
    public class MatriculaBusiness
    {
        private dao.MatriculaDao _MatriculaDao = new dao.MatriculaDao();

        public List<Cursos> ListCurso()
        {
            try
            {
                return _MatriculaDao.ListCurso();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void CadastroAluno(Matricula matricula)
        {
            try
            {
                _MatriculaDao.CadastroAluno(matricula);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

    }
}
