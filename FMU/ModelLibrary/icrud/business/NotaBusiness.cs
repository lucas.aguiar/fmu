﻿using ModelLibrary.nota;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLibrary.icrud.business
{
    public class NotaBusiness
    {
        private dao.NotaDao _dao = new dao.NotaDao();
        public List<nota.Nota> GetNotas(int? re)
        {
            try
            {
                return _dao.getNota(re);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
