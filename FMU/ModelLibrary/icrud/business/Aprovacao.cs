﻿using ModelLibrary.Aprovacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLibrary.icrud.business
{
    public class AprovacaoBusiness
    {
        private dao.AprovacaoDao _dao = new dao.AprovacaoDao();
        public List<Aprovacao.Aprovacao> Aprovacao()
        {
            try
            {
                return _dao.ListAprovacao();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<AprovacaoResult> Aprovacao(List<int> list)
        {
            try
            {
                return _dao.Aprovacao(list);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
