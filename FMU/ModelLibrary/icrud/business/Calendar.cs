﻿using ModelLibrary.Calendar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLibrary.icrud.business
{
    public class Calendar
    {
        private dao.CalendarDao _dao = new dao.CalendarDao();
        public List<PublicHoliday> GetCalendar( int? re)
        {
            try
            {
                return _dao.getCalendar(re);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
