﻿using ModelLibrary.icrud.dao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLibrary.icrud.business
{
    public class GradeBusiness
    {
        GradeDao _dao = new GradeDao();

        public List<Grade.Grade> GetGrade(int re)
        {
            try
            {
                return _dao.GetGrade(re);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
