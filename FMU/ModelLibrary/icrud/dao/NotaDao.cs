﻿using ModelLibrary.Calendar;
using ModelLibrary.nota;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLibrary.icrud.dao
{
    public class NotaDao
    {
        public List<Nota> getNota(int? re )
        {
            var result = new List<Nota>();

            using (var build = new build.NotaBuild())
            {
                using (var cmd = build.MakeNota(re))
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                            result.Add(new Nota
                            {
                                Nome = reader["NOME"].ToString(),
                                Notas = reader["NOTA"].ToString(),
                                Faltas = reader["FALTA"].ToString()
                            });
                    }
                }

            }

            return result;
        }
    }
}
