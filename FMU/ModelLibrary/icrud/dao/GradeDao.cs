﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLibrary.icrud.dao
{
    public class GradeDao
    {

        public List<Grade.Grade> GetGrade(int re)
        {
            var result = new List<Grade.Grade>();

            using (var build = new build.GradeBuild())
            {
                using (var cmd = build.MakeGrade(re))
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                            result.Add(new Grade.Grade
                            {
                                diaSemana = reader["DIA"].ToString(),
                                Aula = reader["AULA"].ToString()
                            });
                    }
                }

            }

            return result;
        }
    }
}
