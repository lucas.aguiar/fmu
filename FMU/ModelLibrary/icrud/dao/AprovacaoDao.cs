﻿using ModelLibrary.Aprovacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLibrary.icrud.dao
{
    public class AprovacaoDao
    {

        public List<Aprovacao.Aprovacao> ListAprovacao()
        {
            var result = new List<Aprovacao.Aprovacao>();

            using (var build = new build.AprovacaoBuild())
            {
                using (var cmd = build.MakeAprovacao())
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                            result.Add(new Aprovacao.Aprovacao
                            {
                                Re = Convert.ToInt32(reader["RE"]),
                                Name = reader["NOME"].ToString()
                            });
                    }
                }

            }

            return result;
        }


        public List<AprovacaoResult> Aprovacao(List<int> list)
        {
            var result = new List<AprovacaoResult>();

            using (var build = new build.AprovacaoBuild())
            {
                using (var cmd = build.MakeAprovacao(list))
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            result.Add(new AprovacaoResult
                            {
                                Email = reader["MAIL"].ToString(),
                                Nome = reader["NOME"].ToString(),
                                Ra = reader["ALN_RA"].ToString()
                            });

                        }

                    }
                }

            }

            return result;

        }


    }
}
