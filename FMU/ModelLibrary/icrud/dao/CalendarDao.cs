﻿using ModelLibrary.Calendar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLibrary.icrud.dao
{
    public class CalendarDao
    {
        public List<PublicHoliday> getCalendar(int? re )
        {
            var result = new List<PublicHoliday>();

            using (var build = new build.CalendarBuild())
            {
                using (var cmd = build.MakeCalendar(re))
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                            result.Add(new PublicHoliday
                            {
                                Start_Date = Convert.ToDateTime(reader["INICIO"]).ToString("yyyy-MM-dd HH:mm:ss"),
                                End_Date = Convert.ToDateTime(reader["FIM"]).ToString("yyyy-MM-dd HH:mm:ss"),
                                Title = reader["TITULO"].ToString(),
                                Flag = Convert.ToInt32(reader["FLAG"])
                            });
                    }
                }

            }

            return result;
        }
    }
}
