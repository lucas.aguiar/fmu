﻿using ModelLibrary.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLibrary.icrud.dao
{
    public class AccountDao
    {
        public int LoginDao(Login model)
        {
            int result = 0;
            using (var build = new build.AccountBuild())
            {
                using (var cmd = build.MakeLogin(model))
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                            result = Convert.ToInt32(reader["RA"]);
                    }
                }

            }

            return result;
        }
    }
}
