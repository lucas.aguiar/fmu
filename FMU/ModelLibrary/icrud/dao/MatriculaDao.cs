﻿using ModelLibrary.Account;
using ModelLibrary.parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLibrary.icrud.dao
{
    public class MatriculaDao
    {
        public List<Cursos> ListCurso()
        {
            var result = new List<Cursos>();

            using (var build = new build.MatriculaBuild())
            {
                using (var cmd = build.MakeListDao())
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                            result.Add(
                                new Cursos
                                {
                                    InsideCodeCurso = Convert.ToInt32(reader["CRS_ID"]),
                                    Name = reader["CRS_NOME"].ToString()
                                });
                    }
                }

            }

            return result;
        }

        public void CadastroAluno(Matricula matricula)
        {
            using (var build = new build.MatriculaBuild())
            {
                using (var cmd = build.MakeCadastroAluno(matricula))
                {
                    cmd.ExecuteNonQuery();
                }

            }
        }

    }
}
