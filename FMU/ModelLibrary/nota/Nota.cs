﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLibrary.nota
{
    public class Nota
    {
        public string Nome { get; set; }
        public string Faltas { get; set; }
        public string Notas { get; set; }
    }
}
