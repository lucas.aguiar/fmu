﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace TkppAPI.IcrudTkppAPI.Helper
{
    class DbHelper : IDisposable
    {
        private SqlConnection _connection;

        public SqlConnection Connection
        {
            get
            {
                if (_connection != null) return _connection;
                _connection = new SqlConnection(ConfigurationManager.AppSettings["connString"]);
                return _connection;///
            }
        }

        public SqlCommand ExecuteTransaction(string procedure)
        {
            var cmd = new SqlCommand();
            cmd.CommandText = procedure;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = Connection;
            cmd.CommandTimeout = 300;

            if (cmd.Connection.State == ConnectionState.Closed)
				cmd.Connection.Open();

            return cmd;
        }

        public SqlCommand ExecuteReport(string procedure)
        {
            var cmd = new SqlCommand();
            cmd.CommandText = procedure;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = Connection;
            cmd.CommandTimeout = 300;

            if (cmd.Connection.State == ConnectionState.Closed) cmd.Connection.Open();

            return cmd;
        }

        public SqlCommand ExecuteTransactionWithoutConnection(string procedure)
        {
            var cmd = new SqlCommand();
            cmd.CommandText = procedure;
            cmd.CommandType = CommandType.StoredProcedure;

            return cmd;
        }
        public SqlCommand ExecuteTransaction(string procedure, List<SqlParameter> parameter)
        {
            var cmd = ExecuteTransaction(procedure);
            cmd.Parameters.AddRange(parameter.ToArray());
            return cmd;
        }

        public SqlCommand ExecuteReport(string procedure, List<SqlParameter> parameter)
        {
            var cmd = ExecuteReport(procedure);
            cmd.Parameters.AddRange(parameter.ToArray());
            return cmd;
        }


        public SqlCommand ExecuteTransactionWithoutConnection(string procedure, List<SqlParameter> parameter)
        {
            var cmd = ExecuteTransactionWithoutConnection(procedure);
            cmd.Parameters.AddRange(parameter.ToArray());
            return cmd;
        }

        public void ExecuteTransaction(List<SqlCommand> command)
        {
            using (var conn = new SqlConnection(ConfigurationManager.AppSettings["connString"]))
            {
                SqlTransaction transaction = null;
                try
                {

                    conn.Open();
                    transaction = conn.BeginTransaction(IsolationLevel.Snapshot);
                    foreach (var comman in command)
                    {
                        comman.Connection = conn;
                        comman.Transaction = transaction;
                        comman.ExecuteNonQuery();
                        comman.Dispose();
                    }

                    transaction.Commit();
                }

                catch (SqlException)
                {
                    transaction?.Rollback();
                    throw;
                }
                catch (Exception)
                {
                    transaction?.Rollback();
                    throw;
                }
                finally
                {
                    conn.Close();
                    SqlConnection.ClearPool(conn);
                }
            }
        }

        public void Dispose()
        {
            _connection?.Dispose();
            if (_connection != null) SqlConnection.ClearPool(_connection);
        }
    }
}
