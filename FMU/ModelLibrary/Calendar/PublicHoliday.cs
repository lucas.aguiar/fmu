﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLibrary.Calendar
{
    public class PublicHoliday
    {
        public int Sr { get; set; }
        public string Title { get; set; }
        public string Desc { get; set; } = string.Empty;
        public string Start_Date { get; set; }
        public string End_Date { get; set; }
        public int Flag { get; set; }
    }
}
