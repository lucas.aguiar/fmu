﻿using ModelLibrary.Calendar;
using ModelLibrary.Email;
using ModelLibrary.Grade;
using ModelLibrary.icrud.business;
using ModelLibrary.nota;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace FMU.Controllers
{
    public class HomeController : Controller
    {

        private Calendar _buss = new Calendar();
        private NotaBusiness _nota = new NotaBusiness();
        private GradeBusiness gradeBusiness = new GradeBusiness();

        public ActionResult Index()
        {

            EmailRequest request = new EmailRequest
            {
                Body = "<h1>teste</h1>",
                Name = "Lucas Aguiar",
                Recipient = "lucas99.abreu@gmail.com",
                Subject = "teste"
            };

            //request.Send(request);

            return View();
        }

        public ActionResult MinhasNotas()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult GradeHoraria()
        {
            var bdList = gradeBusiness.GetGrade(Convert.ToInt32(Session["RA"]));
            var _data = new List<SemanaAula>();


            //{
            //    _data.Add(new SemanaAula
            //    {
            //        Domingo = x.diaSemana.Equals("Domingo", StringComparison.OrdinalIgnoreCase) ? x.Aula : "Não Cadastrada",
            //        Segunda = x.diaSemana.Equals("Segunda-feira", StringComparison.OrdinalIgnoreCase) ? x.Aula : "Não Cadastrada",
            //        Terca = x.diaSemana.Equals("Terça-feira", StringComparison.OrdinalIgnoreCase) ? x.Aula : "Não Cadastrada",
            //        Quarta = x.diaSemana.Equals("Quarta-feira", StringComparison.OrdinalIgnoreCase) ? x.Aula : "Não Cadastrada",
            //        Quinta = x.diaSemana.Equals("Quinta-feira", StringComparison.OrdinalIgnoreCase) ? x.Aula : "Não Cadastrada",
            //        Sexta = x.diaSemana.Equals("Sexta-feira", StringComparison.OrdinalIgnoreCase) ? x.Aula : "Não Cadastrada",
            //        Sabado = x.diaSemana.Equals("Sábado", StringComparison.OrdinalIgnoreCase) ? x.Aula : "Não Cadastrada",

            //    });
            //});

            return View(bdList);
        }

        public ActionResult Calendario()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult GetCalendarData()
        {
            // Initialization.
            JsonResult result = new JsonResult();

            try
            {
                var _data = _buss.GetCalendar(Convert.ToInt32(Session["RA"]));

                // Processing.
                result = this.Json(_data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info
                Console.Write(ex);
            }

            // Return info.
            return result;
        }

        public ActionResult GetNotas()
        {
            // Initialization.
            JsonResult result = new JsonResult();

            try
            {
                var _data = _nota.GetNotas(Convert.ToInt32(Session["RA"]));

                // Processing.
                result = this.Json(_data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info
                Console.Write(ex);
            }

            // Return info.
            return result;
        }

        public JsonResult GetList()
        {
            AprovacaoBusiness business = new AprovacaoBusiness();

            return Json(business.Aprovacao(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AprovarAluno(List<int> re)
        {

            try
            {
                AprovacaoBusiness business = new AprovacaoBusiness();

                var _result = business.Aprovacao(re);

                EmailRequest request = new EmailRequest();


                _result.ForEach(x =>
                {
                    request.Send(new EmailRequest
                    {
                        Subject = $"Aprovação do aluno {x.Nome}",
                        Name = x.Nome,
                        Recipient = x.Email,
                        Body = request.MakeBody(x.Nome, x.Ra)
                    });
                });

                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(new { success = false, error = ex.Message }, JsonRequestBehavior.AllowGet);

            }
        }
    }
}