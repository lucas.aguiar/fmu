﻿using ModelLibrary.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FMU.Controllers
{
    public class AccountController : Controller
    {
        private ModelLibrary.icrud.business.Account _account = new ModelLibrary.icrud.business.Account();
        private ModelLibrary.icrud.business.MatriculaBusiness _matricula = new ModelLibrary.icrud.business.MatriculaBusiness();

        // GET: Account
        public ActionResult Index()
        {
            Login model = new Login();

            return View(model);
        }

        [HttpPost]
        public ActionResult Login(Login model)
        {
            try
            {
                Session.Remove("ra");

                var ra = _account.LoginBusiness(model);

                Session["ra"] = ra;

                return Json(new { success = true, url = "/Home" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(new { success = false, error = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Matricula()
        {
            Matricula model = new Matricula();

            model.Cursos = _matricula.ListCurso();

            return View(model);
        }

        [HttpPost]
        public ActionResult Matricula(Matricula model)
        {

            _matricula.CadastroAluno(model);

            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Sair()
        {
            Session.Remove("ra");

            return RedirectToAction("Index");
        }
    }
}