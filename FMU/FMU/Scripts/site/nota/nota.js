﻿$(document).ready(function () {
    $.ajax({
        url: '/Home/GetNotas',
        type: "GET",
        dataType: "JSON",

        success: function (result) {

            $.each(result, function (i, data) {
                $("#nota").append(`
                    <div class="row">
                        <div class="col-sm-8">
                            <p>${data.Nome}</p>
                        </div>
                        <div class="col-sm-2">
                            <p>${data.Notas}</p>
                        </div>
                        <div class="col-sm-2">
                            <p>${data.Faltas}</p>
                        </div>
                    </div>
                `);
            });
        }
    });
});