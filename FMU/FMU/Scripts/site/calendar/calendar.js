﻿$(document).ready(function () {
    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay,listMonth'
        },
        locale: 'pt-br',
        buttonIcons: false, // show the prev/next text
        weekNumbers: true,
        navLinks: true, // can click day/week names to navigate views
        editable: true,
        eventLimit: true, // allow "more" link when too many events

        events: function (start, end, timezone, callback) {
            $.ajax({
                url: '/Home/GetCalendarData',
                type: "GET",
                dataType: "JSON",

                success: function (result) {
                    var events = [];

                    $.each(result, function (i, data) {
                        events.push({
                            title: data.Title,
                            description: data.Desc,
                            start: moment(data.Start_Date).format('YYYY-MM-DD HH:mm:ss'),
                            end: moment(data.End_Date).format('YYYY-MM-DD HH:mm:ss'),
                            backgroundColor: data.Flag == 1 ? "#ff0000" : "#3a87ad",
                            borderColor: data.Flag == 1 ? "#ff0000" : "#3a87ad"
                        });
                    });

                    callback(events);
                }
            });
        },

        eventRender: function (event, element) {
            element.qtip({
                content: event.description
            });
        },

        editable: false
    });
});