﻿var notification;
var container = document.querySelector('#notification-container');
var visible = false;
var _callback = null;
var queue = [];

function createNotification() {
    notification = document.createElement('div');
    var btn = document.createElement('button');
    var title = document.createElement('div');
    var msg = document.createElement('div');
    btn.className = '_notification-close';
    title.className = '_notification-title';
    msg.className = '_notification-message';
    btn.addEventListener('click', hideNotification, false);
    notification.addEventListener('animationend', hideNotification, false);
    notification.addEventListener('webkitAnimationEnd', hideNotification, false);
    notification.appendChild(btn);
    notification.appendChild(title);
    notification.appendChild(msg);
}

function updateNotification(type, title, message) {
    notification.className = '_notification _notification-' + type;
    notification.querySelector('._notification-title').innerHTML = title;
    notification.querySelector('._notification-message').innerHTML = message;
}

function showNotification(type, title, message, callback = null) {

    if (!notification)
        createNotification();

    updateNotification(type, title, message);
    container = $('#notification-container')[0];
    container.appendChild(notification);
    visible = true;

    _callback = callback;

    if (callback != null)
        $("#notification-container").one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function (e) {
            callback();
        });

}

function hideNotification() {
    if (visible) {

        visible = false;
        container.removeChild(notification);

        if (_callback != null)
            _callback();

        if (queue.length) {
            showNotification.apply(null, queue.shift());
        }
    }
}