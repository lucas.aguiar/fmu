﻿var api = (function () {
    var api = {

        routes: {
            urlList: "/Home/GetList"
        },

        init: function () {
            this.ui.init();
            this.events.init();

        },
        ui: {
            init: function () {},
        },
        events: {
            init: function () {

                this.onClickPending();
                this.onNoDisperse();
                this.onClickAprovar();
            },

            onClickPending: function () {
                $("#btn-aluno-pendente").on("click", function () {

                    $.ajax({
                        type: "GET",
                        contentType: "application/json; charset=utf-8",
                        url: api.routes.urlList,
                        success: function (response) {
                            api.methods.drawnList(response);
                        }
                    });

                });
            },

            onNoDisperse: function () {
                $("ul.nav .dropMessage.dropdown .dropdown-menu").on("click", function (e) {
                    e.stopPropagation();
                });
            },

            onClickAprovar: function () {

                $("#message-container").on("click", "#btn-aprovar", function () {

                    let _arr = [];
                    $(".ipt-check").each(function (i, e) {
                        let _data = $(this).data("re");
                        if ($(this).is(":checked"))
                            _arr.push(_data)
                    });

                    if (_arr.length <= 0) return showNotification("info", "Mensagem", "Selecione o aluno para ser aprovado.");

                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "/Home/AprovarAluno",
                        data: JSON.stringify({
                            re: _arr
                        }),
                        beforeSend: function () {
                            waitingDialog.show('Carregando...', {
                                dialogSize: 'sm',
                                progressType: 'info'
                            });
                        },
                        complete: function () {
                            waitingDialog.hide();
                        },
                        success: function (response) {
                            if (response.success){
                                showNotification("success", "Mensagem", "Aluno(s) Aprovado(s)");
                                $("#message-container").toggle("click");
                            }
                            else
                                showNotification("error", "Mensagem", response.error);

                        }
                    });
                });
            }
        },
        methods: {
            drawnList: function (_arr) {

                $("#message-container").html(`
                    <form id="frm-message">
                    <li>
                        <div class="container-fluid message-list">
                            <div class="row ft-bold">
                                <div class="col-sm-1"></div>
                                <div class="col-sm-5 txt-center"><p>Usuário</p></div>
                                <div class="col-sm-5 txt-center"><p>Nº Registro</p></div>
                            </div>
                    </li>`);


                $.each(_arr, function (i, e) {
                    $(".message-list").append(`   

                        <div class="row row-border">
                            <div class="col-sm-1 txt-center">
                                <input type="checkbox" class="custom-control-input ipt-check" data-re="${e.Re}">
                            </div>
                            <div class="col-sm-5 txt-center">
                                <p>${e.Name}</p>
                            </div>
                            <div class="col-sm-5 txt-center">
                                <p>${e.Re}</p>
                            </div>
                        </div>
                    `);
                });

                $(".message-list").append(`
                    <div class="row">
                        <div class="col-sm-12 padding">
                            <button type="button" class="btn btn-default btn-right" id="btn-aprovar" >Aprovar</button>
                        </div>
                    </div>
                    </div>
                `);
            }
        }
    };

    return {
        init: function () {
            api.init();
        }
    };
})();


$(function () {
    api.init();
});