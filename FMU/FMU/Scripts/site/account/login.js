﻿var api = (function () {
    var api = {

        routes: {
            urlHome: "/Account/Login"
        },

        init: function () {
            this.ui.init();
            this.events.init();

        },
        ui: {
            init: function () {
            },
        },
        events: {
            init: function () {

                this.onClickLogin();
                this.onClickMatricula();
            },

            onClickLogin: function () {
                $("#btn-login").off("click").on("click", function () {

                    if (!$("#frm-login").valid()) return showNotification("error", "Acesso", "Preencha todas as informações");

                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        data: JSON.stringify({ model: api.methods.makeobj() }),
                        url: api.routes.urlHome,
                        beforeSend: function () {
                            waitingDialog.show('Carregando...', { dialogSize: 'sm', progressType: 'info' });
                        },
                        complete: function () {
                            waitingDialog.hide();
                        },
                        success: function (response) {

                            if (!response.success)
                                return showNotification("error", "Acesso", response.error);

                            window.location.href = response.url;
                        }
                    });
                });
            },
            onClickMatricula: function () {
                $("#btn-matricula").on("click", function () {
                    window.location.href = "Account/Matricula/";
                });
            }
        },
        methods: {
            makeobj: function () {
                var model = {

                    Username: $("#Username").val(),
                    Password: $("#Password").val()

                };

                return model;
            }
        }
    };

    return {
        init: function () {
            api.init();
        }
    };
})();


$(function () {
    api.init();
});