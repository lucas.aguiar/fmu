﻿var api = (function () {
    var api = {

        routes: {
            urlMatricula: "/Account/Matricula"
        },

        init: function () {
            this.ui.init();
            this.events.init();

        },
        ui: {
            init: function () {},
        },
        events: {
            init: function () {

                this.onClickMatricula();
            },

            onClickMatricula: function () {
                $("#btn-matricula").off("click").on("click", function () {

                    if (!$("#form-signin-matricula").valid()) return showNotification("error", "Acesso", "Preencha todas as informações");

                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        data: JSON.stringify({
                            model: api.methods.makeobj()
                        }),
                        url: api.routes.urlMatricula,
                        async: false,
                        beforeSend: function () {
                            waitingDialog.show('Carregando...', {
                                dialogSize: 'sm',
                                progressType: 'info'
                            });
                        },
                        complete: function () {
                            waitingDialog.hide();
                        },
                        success: function (response) {

                            if (!response.success)
                                return showNotification("error", "Acesso", response.error);

                            api.methods.cls();
                            
                            showNotification("success", "Matricula", "Cadastro realizado com sucesso.");

                        }
                    });
                });
            },

        },
        methods: {
            makeobj: function () {
                var model = {

                    Nome: $("#Nome").val(),
                    Usuario: $("#Usuario").val(),
                    CursoInsideCode: $("#Cursos option:selected").val(),
                    Senha: $("#Senha").val(),
                    Email: $("#Email").val()

                };

                return model;
            },

            cls: function () {
                $("#Nome").val("");
                $("#Usuario").val("");
                $("#Senha").val("");
                $("#ConfSenha").val("");
                $("#Email").val("");
                $("#Cursos").val("");
            }
        }
    };

    return {
        init: function () {
            api.init();
        }
    };
})();


$(function () {
    api.init();
});